# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 09:26:59 2022

@author: Maeve BLAREL
"""
### TP2 – ANALYSE OBIA
from skimage.io import imread, imsave, imshow
from skimage.segmentation import watershed
import PIL
import numpy as np
import matplotlib.pyplot as plt

## Exercise 2_1  
def createM(imgC, imgS):
    """
    Creates the matrix M. This is a matrix that contains c columns and l rows.
    The number of columns corresponds to the number of segments in the segmentation image imgS.
    The number of rows corresponds to the number of classes in the classification image imgC.
    Each pixel corresponds to the number of elements of a segment s with the value of the class c P(c, s)

    :param imgC: an integer list array of type numpy representing a classification image.
    :type imgC: integer table (numpy.array([[int, int, int,...], [int, int, int,...]]))
    :param imgS: an integer list array of type numpy representing a segmentation image.
    :type imgS: array of float (numpy.array([[float, float, float,...], [float, float, float,...]))
        
    :CU: the size of the parameter images must be the same (np.shape(imgC) == np.shape(imgS)).
    
    :return: a matrix M, an array numpy of size (np.shape(np.unique(imgC)), np.shape(np.unique(imgS))).
    :rtype: a numpy array of float (numpy.array([[float, float, float,...], [float, float, float,...]]))
    """
    valueC = list(np.unique(imgC))
    valueS = list(np.unique(imgS))
    M = np.zeros([len(valueC), len(valueS)])
    for i in range(imgC.shape[0]):
        for j in range(imgC.shape[1]):
            M[imgC[i][j]-1, valueS.index(imgS[i][j])] += 1
    return M


def regClasSeg(imgC, imgS):
    """
    Performs a regularisation of a pixel classification with a segmentation.
    In this function, each pixel in the classification image is reassigned to the majority classification in the segment to which it belongs.
    
    :param imgC: an integer list array of type numpy representing a classification image.
    :type imgC: integer table (numpy.array([[int, int, int,...], [int, int, int,...]]))
    :param imgS: an integer list array of type numpy representing a segmentation image.
    :type imgS: array of float (numpy.array([[float, float, float,...], [float, float, float,...]))
        
    :CU: the size of the parameter images must be the same (np.shape(imgC) == np.shape(imgS)).
    
    :return: a numpy array of size np.shape(imgC) representing the classified image according to the given segmentation.
    :rtype: a numpy array of float (numpy.array([[float, float, float,...], [float, float, float,...]]))
    """

    imageSortie = imgS.copy()
    M = createM(imgC, imgS)
    V = np.argmax(M, axis=0)
    valueSegments = list(np.unique(imgS))
    for i in range(len(valueSegments)):
        imageSortie = np.where(imageSortie==valueSegments[i], V[i]+1, imageSortie)
    return imageSortie


## Exercise 2_2
def moySeg(img, imgS):
    """
    Creates a dictionary calculating the average values R,G and B for segment values.

    :param img: an integer list array of type numpy representing a study image.
    :type img: integer list array (numpy.array([[[int, int, int], [int, int, int], [int, int, int],...], [[int, int, int], [int, int, int], [int, int, int],...]))
    :param imgS: an integer list array of type numpy representing a segmentation image.
    :type imgS: array of float (numpy.array([[float, float, float,...], [float, float, float,...]))
        
    :CU: the size of the parameter images must be the same (np.shape(img)[:2] == np.shape(imgS)[:2]).
    
    :return: a dictionary representing the RGB averages of the pixels present in each segment of the image.
    :rtype: dict (keys = the values of the segments, values = the RGB average of the pixels in this segment)
    """
    valueS = list(np.unique(imgS))
    d_out = dict()
    d = dict()
    for i in range(imgS.shape[0]):
        for j in range(imgS.shape[1]):
            if (imgS[i][j] in d_out):
                d_out[imgS[i][j]] += [img[i][j]]
            else:
                d_out[imgS[i][j]] = [img[i][j]]
    for elt in d_out:
        d[elt] = [np.mean(np.array(d_out[elt])[:,0]), np.mean(np.array(d_out[elt])[:,1]), np.mean(np.array(d_out[elt])[:,2])]
    return d

def regImgRVBSeg(img, imgS):
    """
    Performs an attribute regularisation for a classification.
    In this function, each pixel of the segmentation image is reassigned to the average value of its radiometry in the segment to which it belongs.
 
    :param img: an integer list table of type numpy representing a study image.
    :type img: integer list array (numpy.array([[[int, int, int], [int, int, int], [int, int, int],...], [[int, int, int], [int, int, int], [int, int, int],...]))
    :param imgS: an integer list array of type numpy representing a segmentation image.
    :type imgS: array of float (numpy.array([[float, float, float,...], [float, float, float,...]))
        
    :CU: the size of the parameter images must be the same (np.shape(img)[:2] == np.shape(imgS)[:2]).
    
    :return: a numpy array of size np.shape(imgC) representing the image classified according to the average of the RGB pixel values.
    :rtype: a numpy array of float (numpy.array([[float, float, float,...], [float, float, float,...]]))
    """

    imageSortie = img.copy()
    imageMoy = moySeg(image, imageSeg)
    valueSegments = list(np.unique(imgS))
    for i in range(len(valueSegments)):
        indices = np.where(imgS == valueSegments[i])
        for j in range(len(indices[0])):
            imageSortie[indices[0][j]][indices[1][j]] = imageMoy[valueSegments[i]]
    return imageSortie

def compare(img1, img2, name):
    """
    Compares, in a simple way, two images given as parameters and saves the comparison.
    
    :param img1: an integer list array of type numpy representing the first image to be compared.
    :type img1: integer list array (numpy.array([[[int, int, int], [int, int, int], [int, int, int],...], [[int, int, int], [int, int, int], [int, int, int],...]))
    :param img2: an integer list array of type numpy representing the second image to be compared.
    :type img2: integer list array (numpy.array([[[int, int, int], [int, int, int], [int, int, int],...], [[int, int, int], [int, int, int], [int, int, int],...]))
    :param name: a string representing the name of the image to be saved with its format.
    :type name: string
    
    :CU: the size of the parameter images must be the same (np.shape(img1) == np.shape(img2)).
    
    :return: a numpy array of size np.shape(img1) representing the binary mask of the differences.
    :rtype: a numpy array of integer (numpy.array([[int, int, int,...], [int, int, int,...]]))
    """
    img = img1-img2
    masque = np.where(img>0, 1, img)
    pilim = PIL.Image.fromarray(masque)
    pilim.save(name)
    return masque

def compareClasse(img1, img2, name):
    """
    Compares, from class to class, two images given as parameters and saves the comparison.
    This method is interesting when the number of classes is not too large. The number of comparisons increases exponentially.
    
    :param img1: an integer list array of type numpy representing the first image to be compared.
    :type img1: integer list array (numpy.array([[[int, int, int], [int, int, int], [int, int, int],...], [[int, int, int], [int, int, int], [int, int, int],...]))
    :param img2: an integer list array of type numpy representing the second image to be compared.
    :type img2: integer list array (numpy.array([[[int, int, int], [int, int, int], [int, int, int],...], [[int, int, int], [int, int, int], [int, int, int],...]))
    :param name: a string representing the name of the image to be saved with its format.
    :type name: string
    
    :CU: the size of the parameter images must be the same (np.shape(img1) == np.shape(img2)).
    
    :return: a numpy array of size np.shape(img1) representing the class-to-class mask of the differences.
    :rtype: a numpy array of integer (numpy.array([[int, int, int,...], [int, int, int,...]]))
    """
    img = abs(img1-img2)
    masque = np.where(img==0, 0, img)
    indices = np.where(img!=0)
    for ind in range(len(indices[0])):
        if img1[indices[0][ind]][indices[1][ind]] == 0 and img2[indices[0][ind]][indices[1][ind]] == 1:
            masque[indices[0][ind]][indices[1][ind]] = 1
        elif img1[indices[0][ind]][indices[1][ind]] == 0 and img2[indices[0][ind]][indices[1][ind]] == 2:
            masque[indices[0][ind]][indices[1][ind]] = 2
        elif img1[indices[0][ind]][indices[1][ind]] == 0 and img2[indices[0][ind]][indices[1][ind]] == 3:
            masque[indices[0][ind]][indices[1][ind]] = 3
        elif img1[indices[0][ind]][indices[1][ind]] == 0 and img2[indices[0][ind]][indices[1][ind]] == 4:
            masque[indices[0][ind]][indices[1][ind]] = 4
        elif img1[indices[0][ind]][indices[1][ind]] == 1 and img2[indices[0][ind]][indices[1][ind]] == 0:
            masque[indices[0][ind]][indices[1][ind]] = 5
        elif img1[indices[0][ind]][indices[1][ind]] == 1 and img2[indices[0][ind]][indices[1][ind]] == 2:
            masque[indices[0][ind]][indices[1][ind]] = 6
        elif img1[indices[0][ind]][indices[1][ind]] == 1 and img2[indices[0][ind]][indices[1][ind]] == 3:
            masque[indices[0][ind]][indices[1][ind]] = 7
        elif img1[indices[0][ind]][indices[1][ind]] == 1 and img2[indices[0][ind]][indices[1][ind]] == 4:
            masque[indices[0][ind]][indices[1][ind]] = 8
        elif img1[indices[0][ind]][indices[1][ind]] == 2 and img2[indices[0][ind]][indices[1][ind]] == 0:
            masque[indices[0][ind]][indices[1][ind]] = 9
        elif img1[indices[0][ind]][indices[1][ind]] == 2 and img2[indices[0][ind]][indices[1][ind]] == 1:
            masque[indices[0][ind]][indices[1][ind]] = 10
        elif img1[indices[0][ind]][indices[1][ind]] == 2 and img2[indices[0][ind]][indices[1][ind]] == 3:
            masque[indices[0][ind]][indices[1][ind]] = 11
        elif img1[indices[0][ind]][indices[1][ind]] == 2 and img2[indices[0][ind]][indices[1][ind]] == 4:
            masque[indices[0][ind]][indices[1][ind]] = 12
        elif img1[indices[0][ind]][indices[1][ind]] == 3 and img2[indices[0][ind]][indices[1][ind]] == 0:
            masque[indices[0][ind]][indices[1][ind]] = 13
        elif img1[indices[0][ind]][indices[1][ind]] == 3 and img2[indices[0][ind]][indices[1][ind]] == 1:
            masque[indices[0][ind]][indices[1][ind]] = 14
        elif img1[indices[0][ind]][indices[1][ind]] == 3 and img2[indices[0][ind]][indices[1][ind]] == 2:
            masque[indices[0][ind]][indices[1][ind]] = 15
        elif img1[indices[0][ind]][indices[1][ind]] == 3 and img2[indices[0][ind]][indices[1][ind]] == 4:
            masque[indices[0][ind]][indices[1][ind]] = 16
        elif img1[indices[0][ind]][indices[1][ind]] == 4 and img2[indices[0][ind]][indices[1][ind]] == 0:
            masque[indices[0][ind]][indices[1][ind]] = 17
        elif img1[indices[0][ind]][indices[1][ind]] == 4 and img2[indices[0][ind]][indices[1][ind]] == 1:
            masque[indices[0][ind]][indices[1][ind]] = 18
        elif img1[indices[0][ind]][indices[1][ind]] == 4 and img2[indices[0][ind]][indices[1][ind]] == 2:
            masque[indices[0][ind]][indices[1][ind]] = 19
        elif img1[indices[0][ind]][indices[1][ind]] == 4 and img2[indices[0][ind]][indices[1][ind]] == 3:
            masque[indices[0][ind]][indices[1][ind]] = 20
    pilim = PIL.Image.fromarray(masque)
    pilim.save(name)
    return masque


## Exercise 2_3
def regClasSegEtude(imgC, imgS):
    """
    Performs a regularisation of a pixel classification with a segmentation.
    In this function, each pixel of the classification image is reassigned to the selected classification in the segment to which it belongs.
    Here we determine the classes.
    
    :param imgC: an integer list array of type numpy representing a classification image.
    :type imgC: integer table (numpy.array([[int, int, int,...], [int, int, int,...]]))
    :param imgS: an integer list array of type numpy representing a segmentation image.
    :type imgS: array of float (numpy.array([[float, float, float,...], [float, float, float,...]))
        
    :CU: the size of the parameter images must be the same (np.shape(imgC) == np.shape(imgS)).
    
    :return: a numpy array of size np.shape(imgC) representing the classified image according to the given segmentation.
    :rtype: a numpy array of float (numpy.array([[float, float, float,...], [float, float, float,...]]))
    """
    imageSortie = imgS.copy()
    M = createM(imgC, imgS)
    V = np.argmax(M, axis=0)
    valueSegments = list(np.unique(imgS))
    V = createV(M, V)
    for i in range(len(valueSegments)):
        imageSortie = np.where(imageSortie==valueSegments[i], V[i], imageSortie)
    return imageSortie


def createV(M, V):
    """
    Creates the vector V. This is a vector that contains c columns and 1 rows.
    The number of columns corresponds to the number of segments in the previously used segmentation image imgS.
    This function allows to know which value, which class will be assigned to which segment.
    
    :param M: a matrix of type numpy representing the number of elements of this class for each segment.
    :type M: table of integer (numpy.array([[int, int, int], [int, int, int] ...])
    :param V: a vector of type numpy representing the value to be assigned to each segment.
    :type V: single row integer table (numpy.array([[[int, int, int...]))
        
    :CU: tables M and V have the same number of columns.
    
    :return: a numpy array of size (1, nb_seg) representing the attribute vector for classification.
    :rtype: a numpy table of integer (numpy.array([[int, int, int,...])) with 1 row
    """
    for i in range(np.shape(M)[1]):
        valueMax = np.argmax(M[:,i], axis=0)
        Mpourcentage = M[:,i]/np.sum(M[:,i])
        if Mpourcentage[3] > Mpourcentage[0] and Mpourcentage[0] >= 0.05:
            V[i] = 0
        elif Mpourcentage[3] > Mpourcentage[6] and Mpourcentage[6] >= 0.1:
            V[i] = 6
        elif valueMax == 1 or Mpourcentage[1] >= 0.2:
            V[i] = 1
        elif valueMax == 2 or Mpourcentage[2] >= 0.25:
            V[i] = 2
        elif valueMax == 3 and Mpourcentage[3] >= 0.4:
            V[i] = 3
        elif valueMax == 4 and Mpourcentage[4] >= 0.6:
            V[i] = 4
        elif valueMax == 4 and Mpourcentage[4] < 0.6 or valueMax == 3 and Mpourcentage[3] < 0.45:
            V[i] = 7
    return V
    

def display(img, titre):
    """
    Displays the image given as a parameter according to the given title (display in plots).
    
    :param img: an integer list array or an integer array of type numpy representing the image to display.
    :type img: integer list array or integer array (numpy.array([[[int, int, int], [int, int, int], [int, int, int],...], [[int, int, int], [int, int, int], [int, int, int],...])) or (numpy.array([[int, int, int,...], [int, int, int,...]]))
    :param title: a string representing the title of the image to be displayed.
    :type title: string
    
    :return: none
    """
    plt.figure()
    plt.title(titre, fontsize=8)
    imshow(img)
    plt.show()

    
if __name__ == "__main__":
    # Retrieving the images used in this exercise
    image = imread("Donnees/Donnees_OBIA/Exercice_2_1/ImageIRC.tif")
    imageClassif = imread("Donnees/Donnees_OBIA/Exercice_2_1/IRC_Classif.tif")
    imageSeg = imread("Donnees/Donnees_OBIA/Exercice_2_1/IRC_Segmentation.tif")
    
    imageKolanta = imread("Donnees/Donnees_OBIA/Exercice_2_3/KoLanta_Satellite.tif")
    imageKolantaClassif = imread("Donnees/Donnees_OBIA/Exercice_2_3/KoLanta_classification.tif")
    imageKolantaSeg = imread("Donnees/Donnees_OBIA/Exercice_2_3/KoLanta_segmentation.tif")
    
    
    ## EXERCISE 2.1 : Regularisation of a pixel classification with segmentation :
    imageClassifie = regClasSeg(imageClassif, imageSeg)
    display(imageClassifie, "Exercise 2_1 - Regularisation of a pixel classification with segmentation")
    imsave("img_reg1.tif", imageClassifie)
    print("The regularisation of a pixel classification with a segmentation is performed (exercise 2.1).")
    
    
    ## EXERCISE 2.2 : Regularisation of attributs for a classification :
    # PART I: Each pixel in the segmentation image is reassigned to the average value of its radiometry in the segment to which it belongs.
    imageClassifie = regImgRVBSeg(image, imageSeg)
    display(imageClassifie, "Exercise 2_2 - Part I -  Regularisation of attributs for a classification")
    imsave("img_reg2_1.tif", imageClassifie)
    print("The regularisation of attributes for a classification (exercise 2.2, part I).")
    
    # PART II : The 3-channel image generated in step 1 is classified here.
    # cf image generated via OTB
    
    # COMPARISON OF METHODS: Binary difference map (0/1: same/not the same)
    imageCompare1 = np.array(PIL.Image.open("img_reg1.tif"))
    imageCompare2 = np.array(PIL.Image.open("img_reg2_1.tif")) #img_reg2_2.tif
    comparison1 = compare(imageCompare1, imageCompare2[:,:,2], "binary_compare.tif")   
    
    # METHOD COMPARISON: Class-to-class difference map (1 integer for each transition)
    comparison2 =   compareClasse(imageCompare1, imageCompare2[:,:,2], "classe_compare.tif")
    
    print("Comparisons of the methods by different maps made (exercise 2.2, part II).")


    ## EXERCISE 2.3: Semantic enhancement of a classification : 
    # All pixels in a segment are assigned to a class that depends on the proportion of classes present in the segment.
    imageKolantaClassifie = regClasSegEtude(imageKolantaClassif, imageKolantaSeg)
    display(imageKolantaClassifie, "Exercise 2_3 - Regularisation of a pixel classification with segmentation")
    imsave("imgKolanta_classifie.tif", imageKolantaClassifie)